# vnc-cast

Simple scripts that allow casting from any machine with vnc server to any linux host with vncviewer.

Developed in order to be used with a raspberry pi connected to a tv in our office.

## How to install

1. Install vncviewer on your host (raspbian comes with realvnc pre-installed: This is the default as it allows scaling)
2. Install apache and php or nanoweb. Put `server.php` somewhere in your `httpdocs`.
3. Edit server.php and uncomment the right line depending on whether you have *xtightvncviewer* or *realvnc-vnc-viewer*
4. Install tightvnc (windows) or x11vnc (linux) on the client.
5. Copy client script to your client and give permission to execute `chmod +x filename`.
6. Edit the client script and replace `example.com` with the hostname or ip of your host. 

## How to use

1. Double click on the client script and allow execution.`./filename`
2. If your system is neither linux or windows, you can run your favorite vnc server and just point your browser (or curl) to [http://your.host.nme/path/to/vnc.php](http://your.host.nme/path/to/vnc.php) 
3. To exit, either halt execution `^CTRL-C` of the script or kill your vnc server. It will terminate the session.

